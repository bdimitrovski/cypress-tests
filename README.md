# Minds Cypress Tests

E2E tests for the Minds project, made with [Cypress](https://www.cypress.io/).

## Installation & Usage

Please refer to [our documentation](https://developers.minds.com/docs/walk-throughs/frontend-tests/).

## Contributing

We welcome any contributions to our testbed.
General documentation on contributing to a Minds project can be found [here](https://developers.minds.com/docs/contributing/contributing/).

## License

[AGPLv3](https://gitlab.com/minds/front/-/blob/master/LICENS.md)
