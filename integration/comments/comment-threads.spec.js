import generateRandomId from '../../support/utilities';

/**
 * @author Ben Hayward
 * @create date 2019-08-09 14:42:51
 * @modify date 2019-08-09 14:42:51
 * @desc Spec tests for comment threads.
 */
context.only('Comment Threads', () => {
  const testUsername = generateRandomId();
  const testPassword = generateRandomId() + 'rR.7';
  const postText = generateRandomId();

  const testMessage = {
    1: 'test tier 1',
    2: 'test tier 2',
    3: 'test tier 3',
  };

  const postMenu = 'm-post-menu > button > i';
  const deletePostOption = "m-post-menu > ul > li:visible:contains('Delete')";
  const deletePostButton =
    ".m-modal-confirm-buttons > button:contains('Delete')";

  const commentPostButton = '.m-commentPoster__postButton';
  const textArea = 'minds-textarea';
  const commentBlock = '.minds-block';
  const commentsExpand = '.m-commentsEntityOutlet__expand';

  const viewMore = '.m-comments__viewMore';
  // pass in  tier / tree depth.
  const replyButton = `.m-comment__toolbar > div > span`;
  const commentButton = `minds-button-comment`;
  const commentInput = `m-text-input--autocomplete-container > minds-textarea > div`;
  const commentContent = `m-comments__tree .m-comment__bubble > p`;

  const thumbsUpCounters = '[data-cy=data-minds-thumbs-up-counter]'; //'minds-button-thumbs-up > a > span';
  const thumbsDownCounters = '[data-cy=data-minds-thumbs-down-counter]';

  const thumbsUpButton = '[data-cy=data-minds-thumbs-up-button]';
  const thumbsDownButton = '[data-cy=data-minds-thumbs-down-button]';


  before(() => {
    //make a post new.
    cy.getCookie('minds_sess').then(sessionCookie => {
      if (sessionCookie === null) {
        return cy.login(true);
      }
    });

    // This test makes use of cy.post()
    cy.overrideFeatureFlags({ 'activity-composer': true });
    cy.location('pathname').should('eq', `/newsfeed/subscriptions`);

    cy.post(postText);
  });

  beforeEach(() => {
    cy.preserveCookies();
    cy.server();
    cy.route('GET', '**/api/v2/comments/**').as('commentsOpen');
    cy.route('POST', '**/api/v1/comments/**').as('postComment');
    cy.route('PUT', '**/api/v1/thumbs/**').as('thumbsPut');
  });

  it('should post three tiers of comments', () => {
    // within of post body
    cy.contains(postText)
      .parentsUntil('m-activity')
      .parent()
      .within($list => {
        cy.intercept('POST', '**/api/v1/comments/**').as('POSTComment');

        // type comment
        cy.get(textArea).type("test 1");

        // hit post button.
        cy.get(commentPostButton)
          .click()
          .wait('@POSTComment');

        // within new nested comment block (t2)
        cy.get(commentBlock)
          .within($list => {
            // click reply
            cy.contains('Reply').click();

            // type comment and post
            cy.get(textArea).type("test 2");
            cy.get(commentPostButton).click().wait('@POSTComment');

            // within new nested comment block (t3)
            cy.get(commentBlock)
              .within($list => {
                // click reply
                cy.contains('Reply').click();

                // type comment and post
                cy.get(textArea).type("test 3");
                cy.get(commentPostButton).click().wait('@POSTComment');
              })
          });


      });
  });

  it('should paginate correctly', () => {
    cy.contains(postText)
      .parentsUntil('m-activity')
      .parent()
      .within($list => {
        cy.intercept('POST', '**/api/v1/comments/**').as('POSTComment');

        for (let i = 0; i < 25; i++) {
          cy.get(textArea).last().type(`comment n°${i}`);
          cy.get(commentPostButton).last().click().wait('@POSTComment');
        }

        cy.get('m-relativetimespan').click();
      });

    cy.contains(postText)
      .parentsUntil('m-activity')
      .parent()
      .within($list => {
        cy.route('GET', '**/api/v2/comments/**').as('GETcomments')
        cy.get(viewMore).click().wait('@GETcomments');
        cy.get(viewMore).click().wait('@GETcomments');
      });
  });


});
